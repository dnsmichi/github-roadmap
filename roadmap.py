#!/usr/bin/env python

from github import Github
import os
import csv

REPO_NAME='github/roadmap'
PROJECT_ID=1
CSV_NAME='roadmap.csv'

token = os.getenv('GITHUB_TOKEN')

g = Github(token)

roadmap = g.get_repo(REPO_NAME)
issues = roadmap.get_issues(sort='created')

with open(CSV_NAME, 'w', newline='') as c:
    fieldnames = [ 'gitlab', 'number', 'title', 'labels', 'html_url', 'body' ]
    w = csv.DictWriter(c, fieldnames=fieldnames)

    w.writeheader()

    # Extract and collect all issues
    for i in issues:
        w.writerow({ 'gitlab': '', 'number': i.number, 'title': i.title, 'labels': " ".join([str(l.name) for l in i.labels]), 'html_url': i.html_url, 'body': i.body } )

# Markdown print
print("GitLab | Number | Title | Labels | URL ")
print("-------|--------|-------|--------|-----")

for i in issues:
    #print(" | ".join(['[ ]', str(i.number), i.title, " ".join([str(l.name) for l in i.labels]), i.html_url, i.body ]))
    print(" | ".join(['[ ]', str(i.number), i.title, " ".join([str(l.name) for l in i.labels]), i.html_url]))

# test - docs: https://developer.github.com/v3/projects/columns
#p = roadmap.get_project(PROJECT_ID)
