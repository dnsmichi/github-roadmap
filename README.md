# GitHub Public Roadmap Fetch

## Requirements

```
brew install python3

pip3 install PyGithub
```

Then export your user token into the environment:

```
export GITHUB_TOKEN=xxx
```


## Run

```
python3 roadmap.py
```

This generates the file `roadmap.csv` and prints the content (without the body) as a Markdown table to stdout.


### Example

```
GitLab | Number | Title | Labels | URL
-------|--------|-------|--------|-----
[ ] | 112 | Open Source Documentation for docs.github.com | beta cloud community docs | https://github.com/github/roadmap/issues/112
[ ] | 111 | Role-based Access Control (RBAC) - Custom Roles with fine-grained repo permissions | admin-cloud beta cloud github enterprise | https://github.com/github/roadmap/issues/111
[ ] | 110 | GHES Cluster HA/DR | admin-server ga github enterprise server | https://github.com/github/roadmap/issues/110
[ ] | 109 | Git action events (e.g. clone and push) appear in the audit logs | admin-cloud alpha cloud github enterprise | https://github.com/github/roadmap/issues/109
[ ] | 108 | Scale and optimize highly active repositories | beta code github enterprise | https://github.com/github/roadmap/issues/108
[ ] | 107 | Automerge Pull Requests | all code ga | https://github.com/github/roadmap/issues/107
[ ] | 106 | Secret scanning for private repositories (Server) | ga github advanced security github one security & compliance server | https://github.com/github/roadmap/issues/106
[ ] | 105 | Code scanning (Server) | ga github advanced security github one security & compliance server | https://github.com/github/roadmap/issues/105
[ ] | 104 | GitHub Discussions  | all beta cloud collaboration community | https://github.com/github/roadmap/issues/104
[ ] | 100 | GitHub Actions for GHES (Beta) | actions beta code-to-cloud github enterprise server | https://github.com/github/roadmap/issues/100
[ ] | 99 | Actions: Manual approvals in workflows | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/99
[ ] | 98 | Actions: Centrally managed workflow templates | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/98
[ ] | 97 | Packages: Support for GHES | code-to-cloud ga github enterprise packages server | https://github.com/github/roadmap/issues/97
[ ] | 96 | Actions: Policies for using external actions | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/96
[ ] | 95 | Actions: Multiple hosted runner sizes (Beta) | actions all beta cloud code-to-cloud | https://github.com/github/roadmap/issues/95
[ ] | 94 | Packages: Python (PyPi) support | all cloud code-to-cloud ga packages | https://github.com/github/roadmap/issues/94
[ ] | 93 | Packages: PHP support | all cloud code-to-cloud ga packages | https://github.com/github/roadmap/issues/93
[ ] | 92 | Security Advisories: Traceability | all cloud ga security & compliance | https://github.com/github/roadmap/issues/92
[ ] | 91 | Dependabot Version Updates | all cloud ga security & compliance | https://github.com/github/roadmap/issues/91
[ ] | 90 | Packages: Support for GHES (Beta) | beta code-to-cloud github enterprise packages server | https://github.com/github/roadmap/issues/90
[ ] | 89 | GitHub Actions for GHES | actions code-to-cloud ga github enterprise server | https://github.com/github/roadmap/issues/89
[ ] | 88 | Actions: Workflow visualization | actions beta cloud code-to-cloud github enterprise | https://github.com/github/roadmap/issues/88
[ ] | 87 | Enterprise explore page and improved InnerSource discovery | beta cloud code collaboration | https://github.com/github/roadmap/issues/87
[ ] | 86 | Dependabot Security Updates (server) | beta github enterprise security & compliance server | https://github.com/github/roadmap/issues/86
[ ] | 85 | Code scanning (Server) | beta github advanced security github one security & compliance server | https://github.com/github/roadmap/issues/85
[ ] | 82 | Actions: Share self-hosted runners across multiple organizations in an enterprise account | actions code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/82
[ ] | 81 | Code scanning (Cloud) | cloud ga github advanced security github one security & compliance | https://github.com/github/roadmap/issues/81
[ ] | 80 | Dependency Graph: Review Dependency Changes | beta cloud security & compliance | https://github.com/github/roadmap/issues/80
[ ] | 79 | Dependency Graph: Review Dependency Changes | cloud ga security & compliance | https://github.com/github/roadmap/issues/79
[ ] | 77 | Pages: Private Access | cloud code-to-cloud ga github enterprise pages | https://github.com/github/roadmap/issues/77
[ ] | 75 | Actions: Pull requests from private forks run Actions workflows | actions all cloud code-to-cloud ga | https://github.com/github/roadmap/issues/75
[ ] | 74 | Actions: Use actions from private repositories | actions all cloud code-to-cloud ga | https://github.com/github/roadmap/issues/74
[ ] | 73 | Actions: Self-hosted runner groups | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/73
[ ] | 72 | Actions: GitHub Enterprise Server can use Actions hosted runners | actions code-to-cloud ga github enterprise server | https://github.com/github/roadmap/issues/72
[ ] | 71 | Actions: GitHub Actions on GitHub Private Instances (Beta) | actions beta code-to-cloud github enterprise private-instances | https://github.com/github/roadmap/issues/71
[ ] | 70 | Actions: GitHub Actions on GitHub Private Instances | actions code-to-cloud ga github enterprise private-instances | https://github.com/github/roadmap/issues/70
[ ] | 69 | Packages: GitHub Packages GitHub Private Instances (Beta) | beta code-to-cloud github enterprise packages private-instances | https://github.com/github/roadmap/issues/69
[ ] | 68 | Packages: GitHub Packages on GitHub Private Instances | code-to-cloud ga github enterprise packages private-instances | https://github.com/github/roadmap/issues/68
[ ] | 67 | Dependabot support for private registries | all beta cloud security & compliance | https://github.com/github/roadmap/issues/67
[ ] | 66 | Actions: Increase the cache size for a repo using shared storage | actions all cloud code-to-cloud ga | https://github.com/github/roadmap/issues/66
[ ] | 64 | Actions: GitHub Enterprise Server can support Google Cloud Storage | actions code-to-cloud github enterprise server | https://github.com/github/roadmap/issues/64
[ ] | 63 | Change the default branch name for new repositories from `master` to `main` | admin-cloud admin-server all cloud code | https://github.com/github/roadmap/issues/63
[ ] | 62 | Create a custom list of emails to receive billing threshold notifications | actions all cloud code-to-cloud ga packages | https://github.com/github/roadmap/issues/62
[ ] | 61 | Org admins in Enterprise accounts can view Actions and Packages usage/billing | actions cloud code-to-cloud ga github enterprise packages | https://github.com/github/roadmap/issues/61
[ ] | 60 | Billing API for metered products (i.e. Actions and Packages) | actions all cloud code-to-cloud ga packages | https://github.com/github/roadmap/issues/60
[ ] | 59 | Actions:  Delete workflow runs and archive workflows | actions all cloud code-to-cloud ga | https://github.com/github/roadmap/issues/59
[ ] | 58 | Dependabot Security Updates (server) | ga github enterprise security & compliance server | https://github.com/github/roadmap/issues/58
[ ] | 57 | Secret scanning for private repositories (Server) | beta github advanced security github one security & compliance server | https://github.com/github/roadmap/issues/57
[ ] | 56 | Secret scanning for private repositories (Cloud) | cloud ga github advanced security github one security & compliance | https://github.com/github/roadmap/issues/56
[ ] | 55 | Codespaces, general availability | cloud code ga | https://github.com/github/roadmap/issues/55
[ ] | 54 | Pull request revisions and improved workflows | all beta code | https://github.com/github/roadmap/issues/54
[ ] | 53 | GitHub Private Instances general availability | admin-cloud ga github enterprise private-instances | https://github.com/github/roadmap/issues/53
[ ] | 52 | Actions: Organization and enterprise workflows | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/52
[ ] | 51 | Workflow templates in private repositories | actions cloud code-to-cloud ga github enterprise | https://github.com/github/roadmap/issues/51
```

